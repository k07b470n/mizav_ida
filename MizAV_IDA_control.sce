////////////////////////////////////////////////////////////
//        UBA MizAV - Abfallvermeidungsindikatoren        //
//               INDEX DECOMPOSITION ANALYSIS             //
////////////////////////////////////////////////////////////

// Run the scripts with the following command:
// exec('PATH_TO_THIS_FILE/MizAV_IDA_control.sce');
// NOTE: the input text files (for data and names) needs to follow a given structure. See accompanying Excel file.

mprintf(pwd())

/////////////
// CONTROL //
/////////////

// Name of IDA (e.g 'ANA#3'), can also be a vector
IDA = ['ANA#1', 'ANAind', 'ANAserv', 'HHA#1'];
//IDA = ['ANA#1', 'ANAind', 'ANAserv', 'BAA#1', 'BAA#2', 'BAA#3', 'GWA#2', 'HHA#1', 'HHA#2', 'HHA#3'];

// Path to repo
PathRepo = get_absolute_file_path('MizAV_IDA_control.sce');
// Path to data input
FilePathData = fullfile(PathRepo,'input');
// Path to save results
FilePathRes = fullfile(PathRepo,'output');
// Path to Scilab scripts
FilePathScripts = fullfile(PathRepo,'scripts');

// Number of indicators and parameters (some may not be used for this particular run of IDA)
nbParam = 23;

/////////
// RUN //
/////////

for i=1:size(IDA, 'c')

///////////////////////////////////////////////////////////////////////////////
// ANA - IDA #1 //
//////////////////
if IDA(i) == 'ANA#1' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','ANA_1_names.txt','ANA_1_data.txt','ANA_1_res.txt'];

// Name of indicator to be decomposed
nmInd = 'ANA ohne B&A';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['PW','BVK'];['ANA ohne B&A','PW'];['BVK', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// ANA - IDA #2 //
//////////////////
if IDA(i) == 'ANAind' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_2006-2014.txt','ANAind_names.txt','ANAind_data.txt','ANAind_res.txt'];

// Name of indicator to be decomposed
nmInd = 'ANA ohne B&A ind';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['PWind','PW'];['ANA ohne B&A ind','PWind'];['PW', '1']];

// Time frame
startYr = 2006;
endYr = 2014;
end

if IDA(i) == 'ANAserv' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_2006-2014.txt','ANAserv_names.txt','ANAserv_data.txt','ANAserv_res.txt'];

// Name of indicator to be decomposed
nmInd = 'ANA ohne B&A serv';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['PWserv','PW'];['ANA ohne B&A serv','PWserv'];['PW', '1']];

// Time frame
startYr = 2006;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// BAA - IDA #1 //
//////////////////
if IDA(i) == 'BAA#1' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','BAA_1_names.txt','BAA_1_data.txt','BAA_1_res.txt'];

// Name of indicator to be decomposed
nmInd = 'BAA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 5;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['BVK','HH'];['BAA','PWbau'];['PWbau','PW'];['PW','BVK'];['HH', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// BAA - IDA #2 //
//////////////////
if IDA(i) == 'BAA#2' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','BAA_2_names.txt','BAA_2_data.txt','BAA_2_res.txt'];

// Name of indicator to be decomposed
nmInd = 'BAA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 4;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['BVK','HH'];['NBF','BVK'];['BAA','NBF'];['HH', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// BAA - IDA #3 //
//////////////////
if IDA(i) == 'BAA#3' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','BAA_3_names.txt','BAA_3_data.txt','BAA_3_res.txt'];

// Name of indicator to be decomposed
nmInd = 'BAA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['BAA','PWbau'];['PWbau','PW'];['PW', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// GWA - IDA #2 //
//////////////////
if IDA(i) == 'GWA#2' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','GWA_2_names.txt','GWA_2_data.txt','GWA_2_res.txt'];

// Name of indicator to be decomposed
nmInd = 'GWA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['BIPexp-imp','BWS'];['GWA','BIPexp-imp'];['BWS', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// HHA - IDA #1 //
//////////////////
if IDA(i) == 'HHA#1' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','HHA_1_names.txt','HHA_1_data.txt','HHA_1_res.txt'];

// Name of indicator to be decomposed
nmInd = 'HHA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 4;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['BIPHH','HH'];['HH','BVK'];['HHA','BIPHH'];['BVK', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// HHA - IDA #2 //
//////////////////
if IDA(i) == 'HHA#2' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','HHA_2_names.txt','HHA_2_data.txt','HHA_2_res.txt'];

// Name of indicator to be decomposed
nmInd = 'HHA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['HHA','BIPHH'];['BIPHH','BIP'];['BIP', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end

///////////////////////////////////////////////////////////////////////////////
// HHA - IDA #3 //
//////////////////
if IDA(i) == 'HHA#3' then
mprintf("\n###################### \n")
mprintf("\n" + IDA(i) + "\n")
// File names: 1. parameter names, 2. input data, 3. output file (names), 4. output file (data), 5. output file (names + data)
FileNames = ['MizAV_IDA_names.txt','MizAV_IDA_data_1996-2014.txt','HHA_3_names.txt','HHA_3_data.txt','HHA_3_res.txt'];

// Name of indicator to be decomposed
nmInd = 'HHA';
// Number of determinants (exact number that is required for this particular IDA)
nbDet = 3;
// Matrix of parameter pairs building determinants (ratio of the 1st over the 2nd parameter of each pair)
// Last determinant is not a fraction (ends the IDA)
nmDet = [['HH','BVK'];['HHA','HH'];['BVK', '1']];

// Time frame
startYr = 1996;
endYr = 2014;
end


///////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////

mprintf("\nLoading data \n")
exec(fullfile(FilePathScripts, 'MizAV_IDA_load.sce'));
mprintf("\nCalculating ex-post IDA \n")
exec(fullfile(FilePathScripts, 'MizAV_IDA_expost.sce'));
mprintf("\nExporting results to txt files \n")
exec(fullfile(FilePathScripts, 'MizAV_IDA_output.sce'));

///////////////////////////////////////////////////////////////
end

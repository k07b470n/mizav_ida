# MizAV IDA

This project implements an Index Decomposition Analysis algorithm and applies it to the ex-post decomposition of selected waste flow indicators in Germany.
The code in this repository was developed as part of a research project for the German Environmental Agency.
For the IDA, we use the algorithm proposed by Dietzenbacher and Los (1998) that has the particularity to average all possible decomposition forms.
The mathematical formula and their full derivations are presented in the extensive review of decomposition methods in Hoekstra et al. (2003).

This title of this project comes from:

* The research project's short name [UBA-MizAV](https://wupperinst.org/en/p/wi/p/s/pd/620/) which in turn comes from:
	* UBA or _Umweltbundesamt_, the German Environmental Agency that financed the project
	* _"Geeignete **M**a�st�be und **I**ndikatoren **z**ur Erfolgskontrolle von **A**bfall**v**ermeidungsma�nahmen"_ or _"Appropriate Evaluation Benchmarks and Indicators for Measuring the Success of Waste Prevention Measures"_ in English, the long title for the UBA-funded project.

* The acronym IDA for Index decomposition Analysis, a method to assess the effect of certain driving forces on socio-economic and environmental indicator changes.

## Getting Started

### Get the scripts

You can either download the entire project via the download link on the left in the overview page of the repository or you can clone the repository locally to your computer with:

```
git clone https://k07b470n@bitbucket.org/k07b470n/mizav_ida.git
```

### Prerequisite

You first need to have Scilab installed on your machine, you can get the version you need for your OS from Scilab's [download page](https://www.scilab.org/en/download/6.0.1).

### Running the scripts

After you have downloaded the code and installed Scilab, just start Scilab and give the following command in the console:

```
exec('PATH_TO_THIS_FILE/MizAV_IDA_control.sce');
```

Where you replace _'PATH_TO_THIS_FILE'_ with the path to the project folder you have downloaded.
The results of the IDAs are then stored in the /output folder of the project.
For each indicator decomposed, three text files (semi-column separated) are generated:

* _INDICATOR\_res.txt_ contains all the results of the IDA of INDICATOR
* _INDICATOR\_data.txt_ contains only the numerical data of the _res.txt_ file (i.e. without the first column)
* _INDICATOR\_names.txt_ contains only the first column of the _res.txt_ file

## Built With

[Scilab](https://www.scilab.org/) - Open source software for numerical computation

## Authors

* **Mathieu Saurat** - *Initial work* - [Wuppertal Institute for climate, environment and energy](https://wupperinst.org/en/c/wi/c/s/cd/553/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

This work was supported by the German Environmental Agency (Umweltbundesamt), grant number (Forschungskennzahl) 3715343020.

## References

* Dietzenbacher, E., Los, B. (1998) Structural Decomposition Techniques: sense and sensitivity. _Economic System Research_, 10(4): 307-23.
* Hoekstra R., van der Bergh J.C.J.M. (2003), Comparing structural and index decomposition analysis. _Energy Economics_, 25:39-64.
////////////////////////////////////////////////////////////
//        UBA MizAV - Abfallvermeidungsindikatoren        //
//               INDEX DECOMPOSITION ANALYSIS             //
////////////////////////////////////////////////////////////

//////////////////////////////
// EXPORT DATA TO txt FILES //
//////////////////////////////

//// Values
matRes = [];
// years
matRes = [yrs];
// Original indicator
res = vectorfind(nmParam(:,1), nmInd, 'r');
matRes = [matRes ; matParam(res,:)];
// Delta original indicator
deltaInd = matParam(res, 2:nbC) - matParam(res,1)*ones(1,nbC-1);
matRes = [matRes ; [0, deltaInd]];
// Calculation results
matRes = [matRes ; matDet ; [zeros(nbDet,1), deltaDet] ; matW];

//// Names
nmRes=[];
// years
nmRes = ['years'];
// Indicator
nmRes = [nmRes; nmInd];
// Delta indicator
nmRes = [nmRes; 'delta ( ' + nmInd + ' )'];
// Determinants
for i = 1:nbDet
    nmRes=[nmRes;
        nmDet(i,1) + ' / ' + nmDet(i,2);
    ];
end
// Deltas
for i = 1:nbDet
    nmRes=[nmRes;
        'delta ( ' + nmDet(i,1) + " / " + nmDet(i,2) + ' )';
    ];
end
// Weights
for i = 1:nbDet
    nmRes=[nmRes;
        'weight ( ' + nmDet(i,1) + " / " + nmDet(i,2) + ' )';
    ];
end

// Save results
write_csv(nmRes, fullfile(FilePathRes,FileNames(1,3)), ";");
write_csv(matRes, fullfile(FilePathRes,FileNames(1,4)), ";");
write_csv([nmRes,string(matRes)], fullfile(FilePathRes,FileNames(1,5)), ";");


mprintf("\nDone! \n")

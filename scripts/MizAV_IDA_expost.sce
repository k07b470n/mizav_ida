////////////////////////////////////////////////////////////
//        UBA MizAV - Abfallvermeidungsindikatoren        //
//               INDEX DECOMPOSITION ANALYSIS             //
////////////////////////////////////////////////////////////

////////////////////////
// BUILD DETERMINANTS //
////////////////////////
mprintf("\nBuilding determinants from imported parameters: \n")
disp(nmDet);

numDet = [];
denDet = [];
for i=1:nbDet-1
    res = vectorfind(nmParam(:,1), nmDet(i,1), 'r');
    numDet = [numDet;matParam(res,:)]; // Numerators for building determinants
    res = vectorfind(nmParam(:,1), nmDet(i,2), 'r');
    denDet = [denDet;matParam(res,:)]; // Denominators for building determinants
end

matDet = numDet ./ denDet; // Building ratios (i.e. determinants)

res = vectorfind(nmParam(:,1), nmDet(nbDet,1), 'r');
matDet = [matDet;matParam(res,:)]; // Adding the last determinant (non ratio) to the list of determinants

//////////////////////
// CALCULATE DELTAS //
//////////////////////
mprintf("\nCalculating deltas for each determinant \n")

// For each determinant, for each year compared to the first year in the time series
s = size(matDet);
nbR = s(1); // Number of determinants
nbC = s(2); // Number of years in time series

deltaDet = matDet(:, 2:nbC) - matDet(:,1)*ones(1,nbC-1);

///////////////////////
// CALCULATE WEIGHTS //
///////////////////////
mprintf("\nCalculating weights for each determinant \n")
// after the method described in:
// "Analysis of Changes in Air Emissions in Denmark, 1980-2001", 2003, Peter Rørmose Jensen and Thomas Olsen
//
// For n determinants there are 2^(n-1) equations to calculate weights
// If k (note: 0<=k<n) is the number of subscript 0 (i.e. original year) in a weight, then there are (n-1)! / (n-1-k)!k! such weights.
// For a given k, the corresponding weights include a coefficient (n-1-k)!k!

matW = [];
matDet0 = matDet(:,1) * ones(1,nbC); // Matrix of original (year 0) determinant values

if nbDet == 3 then
    mprintf("\n     IDA with 3 determinants, i.e. 2^(3-1)=4 equations to calculate each weight. \n")

    for i = 1:nbDet
        mprintf("\n     Calculating weights for determinant:   ")
        disp(nmDet(i,:));
        
        if i == 1 then
            submatDet0 = matDet0(2:nbDet,:);
            submatDet = matDet(2:nbDet,:);
        elseif 1 < i & i < nbDet then
            submatDet0 = [matDet0(1:i-1,:);matDet0(i+1:nbDet,:)];
            submatDet = [matDet(1:i-1,:);matDet(i+1:nbDet,:)];
        else
            submatDet0 = matDet0(1:nbDet-1,:);
            submatDet = matDet(1:nbDet-1,:);
        end
    
    // Calculate all 4 elements of weight
    tmpmatW = [];
    tmpmatW = [
        // k=0, 1 equation, coefficient = 2
        2 * prod(submatDet0,'r');
        // k=1, 2 equations, coefficient = 1
        1 * prod([submatDet0(1,:) ; submatDet(2,:)],'r');
        1 * prod([submatDet0(2,:) ; submatDet(1,:)],'r');
        // k=2, 1 equations, coefficient = 2
        2 * prod(submatDet,'r');
    ];
    
    // Sum all elements to calculate one weight per determinant (average by factorial(nbDet))
    matW = [matW; 1/6*sum(tmpmatW,'r')];
    end
end

if nbDet == 4 then
    mprintf("\n     IDA with 4 determinants, i.e. 2^(4-1)=8 equations to calculate each weight. \n")

    for i = 1:nbDet
        mprintf("\n     Calculating weights for determinant:   ")
        disp(nmDet(i,:));
        
        if i == 1 then
            submatDet0 = matDet0(2:nbDet,:);
            submatDet = matDet(2:nbDet,:);
        elseif 1 < i & i < nbDet then
            submatDet0 = [matDet0(1:i-1,:);matDet0(i+1:nbDet,:)];
            submatDet = [matDet(1:i-1,:);matDet(i+1:nbDet,:)];
        else
            submatDet0 = matDet0(1:nbDet-1,:);
            submatDet = matDet(1:nbDet-1,:);
        end
    
    // Calculate all 8 elements of weight
    tmpmatW = [];
    tmpmatW = [
        // k=0, 1 equation, coefficient = 6
        6 * prod(submatDet0,'r');
        // k=1, 3 equations, coefficient = 2
        2 * prod([submatDet0(1,:) ; submatDet([2:3],:)],'r');
        2 * prod([submatDet0(2,:) ; submatDet([1,3],:)],'r');
        2 * prod([submatDet0(3,:) ; submatDet([1:2],:)],'r');
        // k=2, 3 equations, coefficient = 2
        2 * prod([submatDet0([1,2],:) ; submatDet(3,:)],'r');
        2 * prod([submatDet0([1,3],:) ; submatDet(2,:)],'r');
        2 * prod([submatDet0([2,3],:) ; submatDet(1,:)],'r');
        // k=3, 1 equation, coefficient = 6
        6 * prod(submatDet,'r');
    ];
    
    // Sum all elements to calculate one weight per determinant (average by factorial(nbDet))
    matW = [matW; 1/24*sum(tmpmatW,'r')];
    end
end

if nbDet == 5 then
    mprintf("\n     IDA with 5 determinants, i.e. 2^(5-1)=16 equations to calculate each weight. \n")

    for i = 1:nbDet
        mprintf("\n     Calculating weights for determinant:   ")
        disp(nmDet(i,:));
        
        if i == 1 then
            submatDet0 = matDet0(2:nbDet,:);
            submatDet = matDet(2:nbDet,:);
        elseif 1 < i & i < nbDet then
            submatDet0 = [matDet0(1:i-1,:);matDet0(i+1:nbDet,:)];
            submatDet = [matDet(1:i-1,:);matDet(i+1:nbDet,:)];
        else
            submatDet0 = matDet0(1:nbDet-1,:);
            submatDet = matDet(1:nbDet-1,:);
        end
    
    // Calculate all 16 elements of weight
    tmpmatW = [];
    tmpmatW = [
        // k=0, 1 equation, coefficient = 24
        24 * prod(submatDet0,'r');
        // k=1, 4 equations, coefficient = 6
        6 * prod([submatDet0(1,:) ; submatDet([2:4],:)],'r');
        6 * prod([submatDet0(2,:) ; submatDet([1, 3:4],:)],'r');
        6 * prod([submatDet0(3,:) ; submatDet([1:2, 4],:)],'r');
        6 * prod([submatDet0(4,:) ; submatDet([1:3],:)],'r');
        // k=2, 6 equations, coefficient = 4
        4 * prod([submatDet0([1,2],:) ; submatDet([3:4],:)],'r');
        4 * prod([submatDet0([1,3],:) ; submatDet([2, 4],:)],'r');
        4 * prod([submatDet0([1,4],:) ; submatDet([2:3],:)],'r');
        4 * prod([submatDet0([2,3],:) ; submatDet([1, 4],:)],'r');
        4 * prod([submatDet0([2,4],:) ; submatDet([1, 3],:)],'r');
        4 * prod([submatDet0([3,4],:) ; submatDet([1:2],:)],'r');
        // k=3, 4 equations, coefficient = 6
        6 * prod([submatDet(1,:) ; submatDet0([2:4],:)],'r');
        6 * prod([submatDet(2,:) ; submatDet0([1, 3:4],:)],'r');
        6 * prod([submatDet(3,:) ; submatDet0([1:2, 4],:)],'r');
        6 * prod([submatDet(4,:) ; submatDet0([1:3],:)],'r');
        // k=4, 1 equations, coefficient = 24
        24 * prod(submatDet,'r');
    ];
    
    // Sum all elements to calculate one weight per determinant (average by factorial(nbDet))
    matW = [matW; 1/120*sum(tmpmatW,'r')];
    end
end




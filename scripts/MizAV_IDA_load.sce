////////////////////////////////////////////////////////////
//        UBA MizAV - Abfallvermeidungsindikatoren        //
//               INDEX DECOMPOSITION ANALYSIS             //
////////////////////////////////////////////////////////////

// NOTE: the Excel / text files used for data input needs to follow a given structure.

////////////////////////////////
// IMPORT DATA FROM txt FILES //
////////////////////////////////

// Matrix of parameter names and units
nmParam = read_csv(fullfile(FilePathData, FileNames(1,1)), "\t");
// Matrix of parameter values
matParam = read_csv(fullfile(FilePathData, FileNames(1,2)), "\t");
matParam = eval(matParam);
// Year vector
yrs = [startYr:endYr];

// Control data import
s1 = size(nmParam);
s2 = size(matParam);
if s1(1) < 2 | s2(1) < 2 then
    mprintf("\nERROR: change end-of-line character of input txt files to UNIX (LF) format (you can use TextWrangler for that). \n")
end
